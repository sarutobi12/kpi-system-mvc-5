﻿using KPI.Model.DAO;
using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KPI.Model.helpers;
using KPI.Model.ViewModel;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Mail;
using KPI.Model.EF;
using KPI.Web.helpers;
using KPI.Model;
using System.Globalization;
using KPI.Model.ViewModel.EmailViewModel;
using System.Threading;
using PagedList;

namespace KPI.Web.Controllers
{
    [BreadCrumb(Clear = true)]
    public class HomeController : BaseController
    {
        [BreadCrumb(Clear = true)]
        public ActionResult Index()
        {
            BreadCrumb.Add("/", "Home");
            BreadCrumb.SetLabel("Dashboard");
            Thread thread = new Thread(async () => {
                await SendMail();
                #region *) Thông báo để biết gửi mail hay chưa

                await new NotificationDAO().AddSendMail(new StateSendMail());
                new ErrorMessageDAO().Add(new ErrorMessage
                {
                    Function = "Send Mail",
                    Name = "[KPI System] Late on task, [KPI System] Late on upload data"
                });
                #endregion
            });
            thread.Start();
            return View();
        }
        public ActionResult ChangeLanguage(String Langby)
        {
            if (Langby != null)
            {
                var userProfile = Session["UserProfile"] as UserProfileVM;

                var sibars = new UserAdminDAO().Sidebars(userProfile.User.Permission, Langby);
                var userVM = new UserProfileVM();
                userVM.User = userProfile.User;
                userVM.Menus = sibars;

                Session["UserProfile"] = userVM;
                Session["CurrentCulture"] = Langby;
                Request.Cookies["Lang"].Value = Langby;
                new SettingDAO().SetLocalLanguage("LocalLanguage", Langby);

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Langby);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Langby);
            }

            HttpCookie cookie = new HttpCookie("Lang");
            cookie.Value = Langby;
            Response.Cookies.Add(cookie);

            return Redirect(Request.UrlReferrer.ToString());
        }
        #region (*) Method Helper For SendMail()
        public void CheckLateOnUpdate(Tuple<List<object[]>, List<UserViewModel>> auditUploadModel)
        {
            if (auditUploadModel.Item1.Count > 0)
            {
                string host = ConfigurationManager.AppSettings["Http"].ToSafetyString();
                string contentForAuditUpload = System.IO.File.ReadAllText(Server.MapPath("~/Templates/LateOnUpDateData.html"));
                contentForAuditUpload = contentForAuditUpload
                                    .Replace("{{{href}}}", host)
                                    .Replace("{{{content}}}", @"<b style='color:red'>Late On Update Data</b><br/>Your KPIs have expired as below list: ");
                var htmlForUpload = string.Empty;
                var count = 0;


                foreach (var item2 in auditUploadModel.Item1)
                {
                    count++;
                    htmlForUpload += @"<tr>
                            <td valign='top' style='padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;'>{{no}}</td>
                            <td valign='top' style='padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;'>{{area}}</td>
                            <td valign='top' style='padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;'>{{ockpicode}}</td>
                            <td valign='top' style='padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;'>{{kpiname}}</td>
                            <td valign='top' style='padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;'>{{year}}</td>
                            <td valign='top' style='padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;'>{{deadline}}</td>
                             </tr>"
                            .Replace("{{no}}", count.ToSafetyString())
                            .Replace("{{area}}", item2[3].ToSafetyString())
                            .Replace("{{kpiname}}", item2[0].ToSafetyString())
                            .Replace("{{ockpicode}}", item2[2].ToSafetyString())
                            .Replace("{{year}}", item2[4].ToSafetyString())
                            .Replace("{{deadline}}", item2[1].ToSafetyString());
                }
                contentForAuditUpload = contentForAuditUpload.Replace("{{{html-template}}}", htmlForUpload);
                 Commons.SendMailOutlook(auditUploadModel.Item2.Select(x => x.Email).ToList(), "[KPI System-07] Late on upload data", contentForAuditUpload, "Late on upload data");
            }
           
        }

        public void CheckLateOnTask(Tuple<List<object[]>, List<UserViewModel>> model)
        {
            if (model.Item1.Count > 0)
            {
                var count = 0;
                string host = ConfigurationManager.AppSettings["Http"].ToSafetyString();
                var htmlForDeadLine = string.Empty;
                string contentForDeadline = System.IO.File.ReadAllText(Server.MapPath("~/Templates/LateOnTask.html"));
                contentForDeadline = contentForDeadline
                                    .Replace("{{{href}}}", host)
                                    .Replace("{{{content}}}", @"<b style='color:red'>Late On Task</b><br/>Your task have expired as below list: ");
                foreach (var item in model.Item1)
                {
                    //string content = "Please note that the action plan we are overdue on " + item.Deadline;
                    count++;
                    htmlForDeadLine += @"<tr>
                            <td valign='top' style='padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;'>{{no}}</td>
                            <td valign='top' style='padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;'>{{oc}}</td>
                            <td valign='top' style='padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;'>{{kpi}}</td>
                            <td valign='top' style='padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;'>{{task}}</td>
                            <td valign='top' style='padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;'>{{deadline}}</td>
                            <td valign='top' style='padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;'><a href='{{link}}'>Click Here</a></td>
                             </tr>"
                            .Replace("{{no}}", count.ToString())
                            .Replace("{{oc}}", item[3].ToSafetyString())
                            .Replace("{{kpi}}", item[4].ToSafetyString())
                            .Replace("{{task}}", item[0].ToSafetyString())
                            .Replace("{{deadline}}", item[1].ToSafetyString())
                            .Replace("{{link}}", item[2].ToSafetyString());
                }
                contentForDeadline = contentForDeadline.Replace("{{{html-template}}}", htmlForDeadLine);

                Commons.SendMailOutlook(model.Item2.Select(x => x.Email).ToList(), "[KPI System-06] Late on task", contentForDeadline, "Late on task ");
                
            }
           
        }
        #endregion
        public async Task SendMail()
        {
            if (!await new NotificationDAO().IsSend())
            {
                #region *) Field

                var userprofile = Session["UserProfile"] as UserProfileVM;
                string lang = Request.Cookies["Lang"].Value.ToSafetyString();

                var auditUploadModel = new ActionPlanDAO().CheckLateOnUpdateData(userprofile.User.ID, lang);
                var model = new ActionPlanDAO().CheckDeadline(lang);
                #endregion

                #region 1) Late On Update
                CheckLateOnUpdate(auditUploadModel);
                
                #endregion

                #region 2) Check Late On Task
                CheckLateOnTask(model);
                #endregion

               
            }
        }
        public ActionResult UserDashBoard()
        {
            var userprofile = Session["UserProfile"] as UserProfileVM;

            if (userprofile != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        //LateOnUpload
        public ActionResult LateOnUpload(int notificationId, int? page)
        {
            BreadCrumb.Add(Url.Action("Index", "Home"), "Home");
            BreadCrumb.SetLabel("Late On Upload");

            var userprofile = Session["UserProfile"] as UserProfileVM;
            ViewBag.NotificationId = notificationId.ToString();
            return View(new DataChartDAO().LateOnUpLoads(userprofile.User.ID, notificationId).ToPagedList(page ?? 1, 20));
        }

        public async Task<ActionResult> header()
        {
            var userprofile = Session["UserProfile"] as UserProfileVM;
            if (userprofile != null)
            {
                var collection = await new NotificationDAO().NotifyCollection();
                return PartialView(collection);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }

        }

        public async Task<JsonResult> GetNotifications()
        {
            var userprofile = Session["UserProfile"] as UserProfileVM;
            if (userprofile.User.ID == 1)
            {
                return Json(new { arrayID = new List<int>(), total = 0, data = new List<NotificationViewModel>() }, JsonRequestBehavior.AllowGet);

            }
            var listNotifications = await new NotificationDAO().ListNotifications(userprofile.User.ID);
            var total = 0;
            var listID = new List<int>();
            foreach (var item in listNotifications)
            {
                if (item.Seen == false)
                {
                    total++;
                    listID.Add(item.ID);
                }

            }
            return Json(new { arrayID = listID.ToArray(), total = total, data = listNotifications }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ListHistoryNotification()
        {
            var userprofile = Session["UserProfile"] as UserProfileVM;
            if (userprofile == null)
                return Json("", JsonRequestBehavior.AllowGet);
            IEnumerable<NotificationViewModel> model = new NotificationDAO().GetHistoryNotification(userprofile.User.ID);
            return View(model);
        }
        //Upload Successfully
        public async Task<ActionResult> ListSubNotificationDetail(int notificationId, int? page)
        {
            BreadCrumb.Add(Url.Action("Index", "Home"), "Home");
            BreadCrumb.SetLabel("Upload Successfully.");
            var userprofile = Session["UserProfile"] as UserProfileVM;
            return View((await new UploadDAO().GetAllSubNotificationsByIdAsync(notificationId, userprofile.User.ID)).ToPagedList(page ?? 1, 20));
        }

    }
}

