﻿using KPI.Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPI.Model.EF
{
    public class KPILang 
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string LanguageID { get; set; }
        public int KPIID { get; set; }
        [ForeignKey("KPIID")]
        public virtual KPI KPI { get; set; }
    }
}
