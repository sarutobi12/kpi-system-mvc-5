namespace KPI.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class version205 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.ActionPlans", name: "TagID", newName: "CategoryID");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.ActionPlans", name: "CategoryID", newName: "TagID");
        }
    }
}
