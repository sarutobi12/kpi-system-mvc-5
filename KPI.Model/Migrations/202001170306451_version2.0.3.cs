namespace KPI.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class version203 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Targets");
            AddColumn("dbo.Targets", "Code", c => c.String());
            AlterColumn("dbo.Targets", "ID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Targets", "ID");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Targets");
            AlterColumn("dbo.Targets", "ID", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.Targets", "Code");
            AddPrimaryKey("dbo.Targets", "ID");
        }
    }
}
