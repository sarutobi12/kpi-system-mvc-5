namespace KPI.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class version204 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Targets", newName: "WorkingPlan");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.WorkingPlan", newName: "Targets");
        }
    }
}
