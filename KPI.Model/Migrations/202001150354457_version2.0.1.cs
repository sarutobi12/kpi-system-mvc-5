namespace KPI.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class version201 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ActionPlans", "Remark", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ActionPlans", "Remark");
        }
    }
}
