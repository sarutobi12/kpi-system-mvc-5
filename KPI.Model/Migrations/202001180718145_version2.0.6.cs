namespace KPI.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class version206 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.KPILangs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        LanguageID = c.String(),
                        KPIID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.KPIs", t => t.KPIID, cascadeDelete: true)
                .Index(t => t.KPIID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.KPILangs", "KPIID", "dbo.KPIs");
            DropIndex("dbo.KPILangs", new[] { "KPIID" });
            DropTable("dbo.KPILangs");
        }
    }
}
